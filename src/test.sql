PACKAGE BODY A4MFrontOffice AS
 



























































































































































































































































  CPACKAGENAME CONSTANT VARCHAR(40):='A4MFrontOffice';

  CPACKAGEBODYREV     CONSTANT VARCHAR2(100) := '$Rev: 40361 $'; 
  CPACKAGEBODYREVDATE CONSTANT VARCHAR2(100) := '$Date:: 2020-08-03 14:19:35#$'; 

  CTRANCAT_REPRESENTMENTADVICE CONSTANT NUMBER := 43; 
  CTRANCAT_REPRESENTMENT       CONSTANT NUMBER := 44; 
  CTRANCAT_BASEIIADVICE        CONSTANT NUMBER := 53; 

FUNCTION GETEXTRACTENTTYPEPRE(PTLGTYPE IN PLS_INTEGER,
                              PEXTRACTDEVICE IN VARCHAR,
                              PEXTRACTENTCODE IN VARCHAR,
                              PTRANCLASS IN PLS_INTEGER,
                              PDRAFTCAPTURE IN PLS_INTEGER,
                              PORGTLGTYPE  IN PLS_INTEGER,
                              PFTWRAUTORIZED IN BOOLEAN,
                              PTRANCODE IN PLS_INTEGER,
                              PTRANCATEGORY IN NUMBER) RETURN CHAR;  

FUNCTION  BACK2FRONTCRD_STAT(PBACKCRD_STAT  IN CHAR) RETURN PLS_INTEGER IS
  VSTATUS PLS_INTEGER;
  BEGIN
    BEGIN
      VSTATUS:=TO_NUMBER(PBACKCRD_STAT);
    EXCEPTION
      WHEN OTHERS THEN
       VSTATUS:=ASCII(UPPER(PBACKCRD_STAT));
       VSTATUS:=VSTATUS-ASCII('A')+10;
    END;
    RETURN VSTATUS;
  EXCEPTION
   WHEN OTHERS THEN
     ERROR.SAVE('A4MFrontOffice.Back2FrontCRD_STAT');
     RAISE;
  END;


FUNCTION  FRONT2BACKCRD_STAT(PFRONTCRD_STAT IN PLS_INTEGER) RETURN CHAR IS
   VSTAT CHAR(1);
  BEGIN
    BEGIN
      VSTAT:=TO_CHAR(PFRONTCRD_STAT);
    EXCEPTION
     WHEN OTHERS THEN
       IF PFRONTCRD_STAT>15 THEN
         ERROR.RAISEERROR(ERROR.ERRORCONST,'Card status value ('||TO_CHAR(PFRONTCRD_STAT)||') is out of range:1..15 !' );
       END IF;
       VSTAT:=CHR(ASCII('A')+PFRONTCRD_STAT-10);
    END;
    RETURN VSTAT;
  EXCEPTION
   WHEN ERROR.ERROR THEN
     RAISE;
   WHEN OTHERS THEN
     ERROR.SAVE('A4MFrontOffice.Front2BackCRD_STAT');
     RAISE;
  END;




FUNCTION ISREASONDISPUT(PREASON IN PLS_INTEGER) RETURN BOOLEAN
  IS
  BEGIN
    IF PREASON IN (2020,2022)  THEN
      RETURN TRUE;
    END IF;
    IF PREASON >= 4910001 AND PREASON <= 4910099 THEN 
      
      RETURN TRUE;
    END IF;
    IF PREASON>19 AND PREASON<98 THEN
      RETURN TRUE;
    END IF;
    RETURN FALSE;
  END;



FUNCTION  GETTERMTYPENAME(PTERMCLASS IN PLS_INTEGER) RETURN VARCHAR
  IS
  BEGIN
    IF PTERMCLASS=ATMCLASS THEN
      RETURN 'ATM';
    ELSIF PTERMCLASS=POSCLASS THEN
      RETURN 'POS';
    ELSIF PTERMCLASS=SERVICECLASS THEN
      RETURN 'TeleBank';
    ELSIF PTERMCLASS=VOICECLASS THEN
      RETURN 'VOICE';
    ELSIF PTERMCLASS=BANKPOSCLASS THEN
      RETURN 'BankPOS';
    END IF;
    RETURN '???';
  END;



FUNCTION  GETTWOTERMCLASS (PTWRDEVICE IN CHAR,PTRANCODE IN VARCHAR) RETURN PLS_INTEGER IS
  BEGIN
    IF PTWRDEVICE=REFERENCETERMINAL.DEVICE_ATM THEN
      RETURN ATMCLASS;
    ELSIF PTWRDEVICE IN (REFERENCETERMINAL.DEVICE_BANKPOS,REFERENCETERMINAL.DEVICE_POS) THEN
      RETURN POSCLASS;
    ELSIF PTWRDEVICE = REFERENCETERMINAL.DEVICE_VOICE THEN
      RETURN VOICECLASS;
    ELSE
      RETURN SERVICECLASS;
    END IF;
  END;




FUNCTION GETEXTRACTDEVICE(PTERMCLASS IN PLS_INTEGER,PSICCODE IN PLS_INTEGER,PTRANCLASS IN PLS_INTEGER:=NULL, PTRANCODE IN PLS_INTEGER:=NULL) RETURN CHAR
  IS
  BEGIN
    IF PTRANCLASS=TRANFIMICLASS THEN
      RETURN REFERENCETERMINAL.DEVICE_FIMI;
    ELSIF PTRANCLASS=TRANECOMMERCECLASS THEN
      RETURN REFERENCETERMINAL.DEVICE_ECOMMERCE;
    ELSIF PTRANCLASS=TRANCMSCLASS THEN
      RETURN REFERENCETERMINAL.DEVICE_CMS;
    ELSIF PTRANCLASS=TRANMANUALCLASS THEN
      RETURN REFERENCETERMINAL.DEVICE_SYSTEM;
    ELSIF PTRANCLASS = TRANINTERCLASS AND PTRANCODE IN (205, 206) THEN
      RETURN REFERENCETERMINAL.DEVICE_SYSTEM;
    END IF;

    IF PTERMCLASS=ATMCLASS THEN
      RETURN REFERENCETERMINAL.DEVICE_ATM;
    ELSIF PTERMCLASS=POSCLASS THEN
      IF PSICCODE = CBANKSIC THEN
        RETURN REFERENCETERMINAL.DEVICE_BANKPOS;
      END IF;
      RETURN REFERENCETERMINAL.DEVICE_POS;
    ELSIF PTERMCLASS=SERVICECLASS THEN
      RETURN REFERENCETERMINAL.DEVICE_TELEBANK;
    ELSIF PTERMCLASS=VOICECLASS THEN
      RETURN REFERENCETERMINAL.DEVICE_VOICE;
    ELSIF PTERMCLASS=BANKPOSCLASS THEN
      RETURN REFERENCETERMINAL.DEVICE_BANKPOS;
    END IF;

    RETURN NULL;































  END;

FUNCTION  GETEXTRACTDEVICENAME(PTERMCLASS IN PLS_INTEGER,PSICCODE IN PLS_INTEGER,PTRANCLASS IN PLS_INTEGER:=NULL) RETURN VARCHAR
  IS
   VDEVICE CHAR(1);
  BEGIN
    IF PTERMCLASS IS NULL THEN
      RETURN PTERMCLASS;
    END IF;
    VDEVICE:=GETEXTRACTDEVICE(PTERMCLASS,PSICCODE,PTRANCLASS);
    IF    VDEVICE=REFERENCETERMINAL.DEVICE_ATM       THEN RETURN 'ATM';          
    ELSIF VDEVICE=REFERENCETERMINAL.DEVICE_POS       THEN RETURN 'POS';          
    ELSIF VDEVICE=REFERENCETERMINAL.DEVICE_BANKPOS   THEN RETURN 'BankPOS';      
    ELSIF VDEVICE=REFERENCETERMINAL.DEVICE_TELEBANK  THEN RETURN 'TeleBank';     
    ELSIF VDEVICE=REFERENCETERMINAL.DEVICE_VOICE     THEN RETURN 'VOICE';        
    ELSIF VDEVICE=REFERENCETERMINAL.DEVICE_FIMI      THEN RETURN 'FIMI/Service'; 
    ELSIF VDEVICE=REFERENCETERMINAL.DEVICE_ECOMMERCE THEN RETURN 'ECommerce';    
    ELSIF VDEVICE=REFERENCETERMINAL.DEVICE_CMS       THEN RETURN 'CMS';          
    ELSIF VDEVICE='T' THEN
      IF PTRANCLASS=TRANFIMICLASS THEN
        RETURN 'FIMI/Service';
      ELSE
        RETURN 'Manual';
      END IF;
    END IF;
    RETURN '?';
  END;



FUNCTION  GETOPERNAME(PTRANCLASS IN PLS_INTEGER,PTRANCODE IN PLS_INTEGER) RETURN VARCHAR IS
  BEGIN
    IF PTRANCLASS IS NULL THEN
      RETURN 'Transaction class not specified';
    END IF;
    IF PTRANCODE IS NULL THEN
      RETURN 'Transaction code not specified';
    END IF;
    IF PTRANCLASS=TRANATMCLASS THEN 
      IF    PTRANCODE=10  THEN RETURN 'Withdrawal';
      ELSIF PTRANCODE=15  THEN RETURN 'Currency exchange in ATM';
      ELSIF PTRANCODE=20  THEN RETURN 'ATM cash deposit (Deposit)';
      ELSIF PTRANCODE=21  THEN RETURN 'Cardless deposit';
      ELSIF PTRANCODE=22  THEN RETURN 'Change card e-Commerce status via ATM';
      ELSIF PTRANCODE=23  THEN RETURN 'Add new CMS abonent to card/Telebank customer (ATM Add Abonent)'; 
      ELSIF PTRANCODE=24 THEN  RETURN 'Update CMS abonent of card/Telebank customer (ATM Update Abonent)'; 
      ELSIF PTRANCODE=25 THEN  RETURN 'Delete CMS abonent from card/Telebank customer (ATM Delete Abonent)'; 
      ELSIF PTRANCODE=28 THEN  RETURN 'Debit adjustment (ATM Debit Adjustment)';    
      ELSIF PTRANCODE=29 THEN  RETURN 'Credit adjustment (ATM Credit Adjustment)';  
      ELSIF PTRANCODE=30  THEN RETURN 'Balance Inquiry';
      ELSIF PTRANCODE=40  THEN RETURN 'Transfer';
      ELSIF PTRANCODE=42  THEN RETURN 'Debit part of transfer (Transfer Debit)';
      ELSIF PTRANCODE=43  THEN RETURN 'Credit part of transfer (Transfer Credit)';
      ELSIF PTRANCODE=44  THEN RETURN 'Funds transfer to cardless payee';
      ELSIF PTRANCODE=45  THEN RETURN 'Complete funds transfer to cardless payee';
      ELSIF PTRANCODE=50  THEN RETURN 'Payment';
      ELSIF PTRANCODE=52  THEN RETURN 'Debit part of payment (Payment Debit)';
      ELSIF PTRANCODE=53  THEN RETURN 'Credit part of payment (Payment Credit)';
      ELSIF PTRANCODE=54  THEN RETURN 'Cash payment to acquirer vendor';
      ELSIF PTRANCODE=58  THEN RETURN 'Payment reversal with card account credit';
      ELSIF PTRANCODE=60  THEN RETURN 'Message (Message to Institution)';
      ELSIF PTRANCODE=62  THEN RETURN 'ATM Prepaid';
      ELSIF PTRANCODE=63  THEN RETURN 'Debit part of P2P transfer';
      ELSIF PTRANCODE=64  THEN RETURN 'Credit part of P2P transfer';
      ELSIF PTRANCODE=70  THEN RETURN 'Statement print';
      ELSIF PTRANCODE=71  THEN RETURN 'Mini-statement print';
      ELSIF PTRANCODE=72  THEN RETURN 'Report on card transactions';
      ELSIF PTRANCODE=74  THEN RETURN 'Debit part of ATM prepaid (ATM Prepaid Debit)';
      ELSIF PTRANCODE=75  THEN RETURN 'Credit part of ATM prepaid (ATM Prepaid Credit)';
      ELSIF PTRANCODE=77  THEN RETURN 'Cash payment with card verification';
      ELSIF PTRANCODE=81  THEN RETURN 'PIN change';
      ELSIF PTRANCODE=82  THEN RETURN 'Change iPIN';
      ELSIF PTRANCODE=83  THEN RETURN 'Link authentication card to Telebank customer via ATM';
      ELSIF PTRANCODE=84  THEN RETURN 'Link card to Telebank customer via ATM';
      ELSIF PTRANCODE=85  THEN RETURN 'Create virtual card in ATM';
      ELSIF PTRANCODE=86  THEN RETURN 'Change Telebank customer PIN via ATM'; 
      ELSIF PTRANCODE=87  THEN RETURN 'Unblock Offline PIN'; 
      ELSIF PTRANCODE=89  THEN RETURN 'Create Telebank customer';
      ELSIF PTRANCODE=90  THEN RETURN 'Check PIN'; 
      ELSIF PTRANCODE=91  THEN RETURN 'ATM balancing';
      ELSIF PTRANCODE=93  THEN RETURN 'ATM cutover and balancing';
      ELSIF PTRANCODE=96  THEN RETURN 'ATM cash adjustment';
      ELSIF PTRANCODE=97  THEN RETURN 'ATM increase';
      ELSIF PTRANCODE=98  THEN RETURN 'Cash adjustment (Decrease ATM CashIn Hopper)';
      ELSIF PTRANCODE=199 THEN RETURN 'Register or delete alias'; 
      ELSIF PTRANCODE=803 THEN RETURN 'Debit funds within NSPK FPS'; 
      ELSIF PTRANCODE=805 THEN RETURN 'Cancel funds debiting within NSPK FPS'; 
      END IF;
    ELSIF PTRANCLASS=TRANPOSCLASS THEN 
      IF    PTRANCODE=110 THEN RETURN 'Purchase';
      ELSIF PTRANCODE=111 THEN RETURN 'Pre-purchase';
      ELSIF PTRANCODE=112 THEN RETURN 'Pre-authorization complete';
      ELSIF PTRANCODE=113 THEN RETURN 'Purchase by way of Internet (POS Mail/Phone Order)';
      ELSIF PTRANCODE=114 THEN RETURN 'Merchandise return';
      ELSIF PTRANCODE=115 THEN RETURN 'Cash advance';
      ELSIF PTRANCODE=116 THEN RETURN 'Card verification';
      ELSIF PTRANCODE=117 THEN RETURN 'POS Balance Inquiry';
      ELSIF PTRANCODE=118 THEN RETURN 'Purchase with change (Purchase with Cashback)';
      ELSIF PTRANCODE=121 THEN RETURN 'Purchase adjustment';
      ELSIF PTRANCODE=122 THEN RETURN 'Merchandize return adjustment';
      ELSIF PTRANCODE=123 THEN RETURN 'Cash advance adjustment';
      ELSIF PTRANCODE=124 THEN RETURN 'Preauthorization adjustment (Pre-purchase Increment)'; 
      ELSIF PTRANCODE=125 THEN RETURN 'Cancel purchase';
      ELSIF PTRANCODE=126 THEN RETURN 'Message to FI via POS';
      ELSIF PTRANCODE=128 THEN RETURN 'Funds transfer to cardless payee';
      ELSIF PTRANCODE=129 THEN RETURN 'Complete funds transfer to cardless payee';
      ELSIF PTRANCODE=130 THEN RETURN 'Quasi-Cash purchase';
      ELSIF PTRANCODE=131 THEN RETURN 'POS Prepaid';
      ELSIF PTRANCODE=132 THEN RETURN 'Debit part of P2P transfer';
      ELSIF PTRANCODE=133 THEN RETURN 'Credit part of P2P transfer';
      ELSIF PTRANCODE=136 THEN RETURN 'POS Installment Details' ; 
      ELSIF PTRANCODE=137 THEN RETURN 'Funds transfer in POS terminal';
      ELSIF PTRANCODE=138 THEN RETURN 'Print mini-statement on account operations';
      ELSIF PTRANCODE=139 THEN RETURN 'Change PIN';
      ELSIF PTRANCODE=140 THEN RETURN 'POS Deposit';
      ELSIF PTRANCODE=141 THEN RETURN 'POS Deposit Adjustment';
      ELSIF PTRANCODE=142 THEN RETURN 'POS payment';
      ELSIF PTRANCODE=147 THEN RETURN 'Debit part of transfer (Transfer Debit)';   
      ELSIF PTRANCODE=148 THEN RETURN 'Credit part of transfer (Transfer Credit)'; 
      ELSIF PTRANCODE=153 THEN RETURN 'Create Telebank customer via POS terminal';
      ELSIF PTRANCODE=154 THEN RETURN 'Link card to Telebank customer';
      ELSIF PTRANCODE=155 THEN RETURN 'Link authentication card to Telebank customer';
      ELSIF PTRANCODE=156 THEN RETURN 'Add new CMS abonent to card/Telebank customer (POS Add Abonent)'; 
      ELSIF PTRANCODE=157 THEN RETURN 'Update CMS abonent of card/Telebank customer (POS Update Abonent)'; 
      ELSIF PTRANCODE=158 THEN RETURN 'Delete CMS abonent from card/Telebank customer (POS Delete Abonent)'; 
      ELSIF PTRANCODE=159 THEN RETURN 'Change PIN of Telebank customer via terminal (Telebank PIN Change via POS)'; 
      ELSIF PTRANCODE=160 THEN RETURN 'Close POS batch of transactions (Close Packet)';
      ELSIF PTRANCODE=162 THEN RETURN 'Close day for POS terminals';
      ELSIF PTRANCODE=168 THEN RETURN 'Debit adjustment (POS Debit Adjustment)';    
      ELSIF PTRANCODE=169 THEN RETURN 'Credit adjustment (POS Credit Adjustment)';  
      ELSIF PTRANCODE=172 THEN RETURN 'Debit part of POS prepaid (POS Prepaid Debit)';
      ELSIF PTRANCODE=173 THEN RETURN 'Credit part of POS prepaid (POS Prepaid Credit)';
      ELSIF PTRANCODE=176 THEN RETURN 'Debit part of payment (Payment Debit)';
      ELSIF PTRANCODE=178 THEN RETURN 'Request for possibility to generate token';  
      ELSIF PTRANCODE=179 THEN RETURN 'Send dynamic password for token generation';  
      ELSIF PTRANCODE=177 THEN RETURN 'Credit part of payment (Payment Credit)';
      ELSIF PTRANCODE=181 THEN RETURN 'Accumulate bonuses via POS terminal';
      ELSIF PTRANCODE=182 THEN RETURN 'Spend bonuses via POS terminal';
      ELSIF PTRANCODE=183 THEN RETURN 'Cash payment to acquirer vendor via POS terminal'; 
      ELSIF PTRANCODE=187 THEN RETURN 'Create virtual card in POS terminal'; 
      ELSIF PTRANCODE=188 THEN RETURN 'Complete token generation';  
      ELSIF PTRANCODE=189 THEN RETURN 'Change token';  
      ELSIF PTRANCODE=197 THEN RETURN 'Change Internet PIN-code in POS-terminal'; 
      ELSIF PTRANCODE=199 THEN RETURN 'Register or delete alias'; 
      ELSIF PTRANCODE=803 THEN RETURN 'Debit funds within NSPK FPS'; 
      ELSIF PTRANCODE=804 THEN RETURN 'Credit funds within NSPK FPS'; 
      ELSIF PTRANCODE=805 THEN RETURN 'Cancel funds debiting within NSPK FPS'; 
      ELSIF PTRANCODE=806 THEN RETURN 'Cancel funds crediting within NSPK FPS'; 
      ELSIF PTRANCODE=807 THEN RETURN 'Complete cancellation of funds crediting within NSPK FPS'; 
      ELSIF PTRANCODE=823 THEN RETURN 'Debit funds within NSPK FPS'; 
      ELSIF PTRANCODE=824 THEN RETURN 'Credit funds within NSPK FPS'; 
      ELSIF PTRANCODE=829 THEN RETURN 'Refund for operation of crediting funds within NSPK FPS'; 
      ELSIF PTRANCODE=830 THEN RETURN 'Refund for operation of debiting funds within NSPK FPS'; 
      ELSIF PTRANCODE=832 THEN RETURN 'Credit retailer upon QR purchase'; 
      END IF;
    ELSIF PTRANCLASS = TRANINTERCLASS THEN 
      IF    PTRANCODE=201 THEN  RETURN 'Debit adjustment';
      ELSIF PTRANCODE=202 THEN  RETURN 'Credit adjustment';
      ELSIF PTRANCODE=205 THEN  RETURN 'Fee Collection';
      ELSIF PTRANCODE=206 THEN  RETURN 'Funds disbursement';
      ELSIF PTRANCODE=210 THEN  RETURN 'Retrieval Request';
      ELSIF PTRANCODE=211 THEN  RETURN 'Retrieval Request Response';
      ELSIF PTRANCODE=212 THEN  RETURN 'Request for Retrieval Request Reversal';
      ELSIF PTRANCODE=213 THEN  RETURN 'Request for Retrieval Response Reversal';
      ELSIF PTRANCODE=214 THEN  RETURN 'Request for Fee Chargeback';
      ELSIF PTRANCODE=215 THEN  RETURN 'Request for Second Fee Presentment';
      ELSIF PTRANCODE=216 THEN  RETURN 'Request for Second Fee Chargeback';
      ELSIF PTRANCODE=217 THEN  RETURN 'Request for Fee Chargeback Reversal';
      END IF;
    ELSIF PTRANCLASS=TRANTBCLASS THEN 
      IF    PTRANCODE=301 THEN  RETURN 'Telebank logon';
      ELSIF PTRANCODE=302 THEN  RETURN 'Get account list with balances';
      ELSIF PTRANCODE=303 THEN  RETURN 'Get operations history';
      ELSIF PTRANCODE=304 THEN  RETURN 'Change customer acct description';
      ELSIF PTRANCODE=305 THEN  RETURN 'Telebank Balance Inquiry';
      ELSIF PTRANCODE=306 THEN  RETURN 'Change Telebank customer CMS address';
      ELSIF PTRANCODE=307 THEN  RETURN 'Change card CMS address';
      ELSIF PTRANCODE=308 THEN  RETURN 'Change card to account link description';
      ELSIF PTRANCODE=309 THEN  RETURN 'Change card or Telebank customer (Set Card Limits) limits';
      ELSIF PTRANCODE=310 THEN  RETURN 'Get full vendors list';
      ELSIF PTRANCODE=311 THEN  RETURN 'Get customer vendors list';
      ELSIF PTRANCODE=312 THEN  RETURN 'Attach vendor to customer';
      ELSIF PTRANCODE=313 THEN  RETURN 'Detach vendor from customer';
      ELSIF PTRANCODE=315 THEN  RETURN 'Link vendor to card';
      ELSIF PTRANCODE=316 THEN  RETURN 'Unlink vendor from card';
      ELSIF PTRANCODE=317 THEN  RETURN 'Change text alias (Change Text Login)';
      ELSIF PTRANCODE=318 THEN  RETURN 'Change numeric alias (Change Numeric Login)';
      ELSIF PTRANCODE=320 THEN  RETURN 'Get payment/transfer schedule';
      ELSIF PTRANCODE=321 THEN  RETURN 'Get explain payment/transfer schedule';
      ELSIF PTRANCODE=322 THEN  RETURN 'Get payment/transfer history';
      ELSIF PTRANCODE=324 THEN  RETURN 'Assign default address for dynamic authentication';
      ELSIF PTRANCODE=330 THEN  RETURN 'Change customer password (Telebank PIN Change)';
      ELSIF PTRANCODE=331 THEN  RETURN 'Get list of customer cards with states (Card list)';
      ELSIF PTRANCODE=332 THEN  RETURN 'Card lock';
      ELSIF PTRANCODE=333 THEN  RETURN 'Card activation';
      ELSIF PTRANCODE=334 THEN  RETURN 'Change card alias';
      ELSIF PTRANCODE=335 THEN  RETURN 'Get account-linked cards';
      ELSIF PTRANCODE=336 THEN  RETURN 'Create virtual card';
      ELSIF PTRANCODE=337 THEN  RETURN 'Change card`s iPIN';
      ELSIF PTRANCODE=340 THEN  RETURN 'Telebank schedule';
      ELSIF PTRANCODE=341 THEN  RETURN 'Telebank cancel schedule';
      ELSIF PTRANCODE=349 THEN  RETURN 'Funds transfer to cardless payee via Telebank';
      ELSIF PTRANCODE=351 THEN  RETURN 'Telebank transfer';
      ELSIF PTRANCODE=352 THEN  RETURN 'Telebank payment';
      ELSIF PTRANCODE=353 THEN  RETURN 'Telebank Prepaid';
      ELSIF PTRANCODE=354 THEN  RETURN 'Transfer to external acct or card (Telebank Outside Transfer)';
      ELSIF PTRANCODE=355 THEN  RETURN 'Debit part of P2P transfer';
      ELSIF PTRANCODE=356 THEN  RETURN 'Credit part of P2P transfer';
      ELSIF PTRANCODE=363 THEN  RETURN 'Card statement request';
      ELSIF PTRANCODE=364 THEN  RETURN 'Change card e-Commerce status via Telebank'; 
      ELSIF PTRANCODE=366 THEN  RETURN 'Set level of Telebank customer additional authentication';
      ELSIF PTRANCODE=367 THEN  RETURN 'Set address for delivery of dynamic passwords for dynamic authentication in TWSG';
      ELSIF PTRANCODE=369 THEN  RETURN 'Debit part of transfer (Transfer Debit)';
      ELSIF PTRANCODE=370 THEN  RETURN 'Credit part of transfer (Transfer Credit)';
      ELSIF PTRANCODE=374 THEN  RETURN 'Set user felds of Telebank customer or private customer';
      ELSIF PTRANCODE=375 THEN  RETURN 'Message (Message to Institution)';
      ELSIF PTRANCODE=376 THEN  RETURN 'Set alphanumeric password for Telebank customer';
      ELSIF PTRANCODE=377 THEN  RETURN 'Set alphanumeric password for Telebank customer card';
      ELSIF PTRANCODE=378 THEN  RETURN 'Calculate CVV2 for virtual card';
      ELSIF PTRANCODE=380 THEN  RETURN 'Payment reversal with card account credit'; 
      ELSIF PTRANCODE=383 THEN  RETURN 'Debit part of payment via Telebank';  
      ELSIF PTRANCODE=384 THEN  RETURN 'Credit part of payment via Telebank'; 
      ELSIF PTRANCODE=388 THEN  RETURN 'Add new CMS abonent to card/Telebank customer';
      ELSIF PTRANCODE=389 THEN  RETURN 'Update CMS abonent of card/Telebank customer';
      ELSIF PTRANCODE=390 THEN  RETURN 'Delete CMS abonent from card/Telebank customer';
      ELSIF PTRANCODE=1302 THEN RETURN 'Change card PIN via Telebank';
      ELSIF PTRANCODE=1304 THEN RETURN 'Create Telebank customer via Telebank';
      ELSIF PTRANCODE=1305 THEN RETURN 'Link supplementary card to Telebank customer';
      ELSIF PTRANCODE=1306 THEN RETURN 'Link authentication card to Telebank customer';
      ELSIF PTRANCODE=1310 THEN RETURN 'Set account limits via Telebank'; 
      ELSIF PTRANCODE=1322 THEN RETURN 'Change card authentication settings for e-Commerce program'; 
      ELSIF PTRANCODE=1325 THEN RETURN 'Configure abonent dynamic profile'; 
      ELSIF PTRANCODE=1332 THEN RETURN 'Set customer limits';  
      ELSIF PTRANCODE=1334 THEN RETURN 'Change state of card contactless interface';  
      ELSIF PTRANCODE=1347 THEN RETURN 'Register card in wallet';  
      ELSIF PTRANCODE=199 THEN RETURN 'Register or delete alias'; 
      ELSIF PTRANCODE=803 THEN RETURN 'Debit funds within NSPK FPS'; 
      ELSIF PTRANCODE=805 THEN RETURN 'Cancel funds debiting within NSPK FPS'; 
      ELSIF PTRANCODE=823 THEN RETURN 'Debit funds within NSPK FPS'; 
      ELSIF PTRANCODE=830 THEN RETURN 'Refund for operation of debiting funds within NSPK FPS'; 
      END IF;
    ELSIF PTRANCLASS = TRANMANUALCLASS THEN 
      IF    PTRANCODE=402 THEN  RETURN 'Change card status';
      ELSIF PTRANCODE=405 THEN  RETURN 'Change temporary card limit';      
      ELSIF PTRANCODE=414 THEN  RETURN 'Set temporary overdraft on account'; 
      ELSIF PTRANCODE=416 THEN  RETURN 'Change account status';
      ELSIF PTRANCODE=417 THEN  RETURN 'Change account status in account-to-card link';
      ELSIF PTRANCODE=423 THEN  RETURN 'Change abonent data'; 
      ELSIF PTRANCODE=425 THEN  RETURN 'Add abonent profile'; 
      ELSIF PTRANCODE=440 THEN  RETURN 'Delete abonent profile'; 
      ELSIF PTRANCODE=442 THEN  RETURN 'Change abonent profile'; 
      ELSIF PTRANCODE=443 THEN  RETURN 'Change card e-Commerce status'; 
      ELSIF PTRANCODE=444 THEN  RETURN 'Change card limits'; 
      ELSIF PTRANCODE=446 THEN  RETURN 'Manage stop-list of VISA via customer service terminal';       
      ELSIF PTRANCODE=447 THEN  RETURN 'Manage stop-list of MasterCard via customer service terminal'; 
      ELSIF PTRANCODE=460 THEN  RETURN 'Reset card IPVV'; 
      ELSIF PTRANCODE=467 THEN  RETURN 'Change card authentication settings for e-Commerce program'; 
      ELSIF PTRANCODE=472 THEN  RETURN 'Add card external ID';  
      ELSIF PTRANCODE=473 THEN  RETURN 'Delete card external ID';  
      ELSIF PTRANCODE=482 THEN  RETURN 'Change allowed excess of available account balance';  
      ELSIF PTRANCODE=486 THEN  RETURN 'Change counter';  
      ELSIF PTRANCODE=487 THEN  RETURN 'Change state of card contactless interface';  
      END IF;
    ELSIF PTRANCLASS = TRANFIMICLASS THEN 
      IF    PTRANCODE=199 THEN  RETURN 'Register or delete alias'; 
      ELSIF PTRANCODE=504 THEN  RETURN 'Specify account description in account-to-card link';
      ELSIF PTRANCODE=506 THEN  RETURN 'Set parameters of message exchange by card';
      ELSIF PTRANCODE=507 THEN  RETURN 'Define parameters of risks control for card';
      ELSIF PTRANCODE=508 THEN  RETURN 'Link card to account';
      ELSIF PTRANCODE=509 THEN  RETURN 'Assign card profile';
      ELSIF PTRANCODE=512 THEN  RETURN 'Set card status';
      ELSIF PTRANCODE=514 THEN  RETURN 'Change card limits';
      ELSIF PTRANCODE=515 THEN  RETURN 'Assign card PVV';
      ELSIF PTRANCODE=516 THEN  RETURN 'Reset bad PIN tries counter';
      ELSIF PTRANCODE=517 THEN  RETURN 'Assign expiration date';
      ELSIF PTRANCODE=522 THEN  RETURN 'Set account status';
      ELSIF PTRANCODE=525 THEN  RETURN 'Create acct';
      ELSIF PTRANCODE=527 THEN  RETURN 'Set account status for card-to-account link';
      ELSIF PTRANCODE=528 THEN  RETURN 'Debit acct';
      ELSIF PTRANCODE=529 THEN  RETURN 'Credit account';
      ELSIF PTRANCODE=530 THEN  RETURN 'Relink card to account';
      ELSIF PTRANCODE=535 THEN  RETURN 'Set card-to-account link limits';
      ELSIF PTRANCODE=536 THEN  RETURN 'Create virtual card';
      ELSIF PTRANCODE=538 THEN  RETURN 'Set account limits';
      ELSIF PTRANCODE=552 THEN  RETURN 'Command to POS terminal';
      ELSIF PTRANCODE=560 THEN  RETURN 'Set level of Telebank customer additional authentication';
      ELSIF PTRANCODE=568 THEN  RETURN 'Link vendor to card';
      ELSIF PTRANCODE=569 THEN  RETURN 'Unlink vendor from card';
      ELSIF PTRANCODE=580 THEN  RETURN 'Manage user fields';
      ELSIF PTRANCODE=586 THEN  RETURN 'Manage VISA stoplist via FIMI';
      ELSIF PTRANCODE=587 THEN  RETURN 'Manage MasterCard stoplist via FIMI';
      ELSIF PTRANCODE=588 THEN  RETURN 'Manage local stoplist via FIMI';
      ELSIF PTRANCODE=592 THEN  RETURN 'Create Telebank customer';
      ELSIF PTRANCODE=598 THEN  RETURN 'Configure abonent profile'; 
      ELSIF PTRANCODE=1502 THEN RETURN 'Create private customer';       
      ELSIF PTRANCODE=1504 THEN RETURN 'Add customer secret question/answer'; 
      ELSIF PTRANCODE=1505 THEN RETURN 'Delete customer secret question/answer';   
      ELSIF PTRANCODE=1506 THEN RETURN 'Modify customer secret question/answer';   
      ELSIF PTRANCODE=1518 THEN RETURN 'Change card e-Commerce status via FIMI'; 
      ELSIF PTRANCODE=1529 THEN RETURN 'Update information on retailer'; 
      ELSIF PTRANCODE=1542 THEN RETURN 'Update private customer data';       
      ELSIF PTRANCODE=1551 THEN RETURN 'Change cardholder';
      ELSIF PTRANCODE=1552 THEN RETURN 'Change account holder';
      ELSIF PTRANCODE=1553 THEN RETURN 'Change overdraft parameters'; 
      ELSIF PTRANCODE=1554 THEN RETURN 'Add link TB <- Retailer';
      ELSIF PTRANCODE=1555 THEN RETURN 'Delete link TB <- Retailer';
      ELSIF PTRANCODE=1561 THEN RETURN 'Update information on terminal'; 
      ELSIF PTRANCODE=1564 THEN RETURN 'Link card to Telebank customer';        
      ELSIF PTRANCODE=1565 THEN RETURN 'Delete card-to-Telebank customer link'; 
      ELSIF PTRANCODE=1580 THEN RETURN 'Change card authentication settings for e-Commerce program'; 
      ELSIF PTRANCODE=1596 THEN RETURN 'Delete card-to-account link'; 
      ELSIF PTRANCODE=1599 THEN RETURN 'Add new CMS abonent to customer';  
      ELSIF PTRANCODE=2500 THEN RETURN 'Delete CMS abonent from customer';  
      ELSIF PTRANCODE=2501 THEN RETURN 'Update CMS abonent of customer';  
      ELSIF PTRANCODE=2502 THEN RETURN 'Configure profile of CMS abonent of customer';  
      ELSIF PTRANCODE=2514 THEN RETURN 'Change numeric or alphabetic login of Telebank customer'; 
      ELSIF PTRANCODE=2521 THEN RETURN 'Set customer limits';  
      ELSIF PTRANCODE=2526 THEN RETURN 'Change customer counters';  
      ELSIF PTRANCODE=2528 THEN RETURN 'Change account counters';  
      ELSIF PTRANCODE=2530 THEN RETURN 'Change cards counters';  
      ELSIF PTRANCODE=2532 THEN RETURN 'Change state of card contactless interface';  
      ELSIF PTRANCODE=2545 THEN RETURN 'Register card in wallet';  
      ELSIF PTRANCODE=2558 THEN RETURN 'Set ranges of card/Telebank customer limit';  
      ELSIF PTRANCODE=2559 THEN RETURN 'Set ranges of account limit';  
      ELSIF PTRANCODE=2560 THEN RETURN 'Set ranges of customer limit';  
      ELSIF PTRANCODE=2562 THEN RETURN 'Add arrest';  
      ELSIF PTRANCODE=2563 THEN RETURN 'Delete arrest';  
      ELSIF PTRANCODE=2564 THEN RETURN 'Change arrest';  
      ELSIF PTRANCODE=2565 THEN RETURN 'Change allowed excess of available account balance';  
      END IF;
    ELSIF PTRANCLASS = TRANECOMMERCECLASS THEN
      IF    PTRANCODE=602 THEN  RETURN 'Payer 3-D Secure authentication';
      ELSIF PTRANCODE=606 THEN  RETURN '3-D Secure Add card abonent'; 
      ELSIF PTRANCODE=607 THEN  RETURN '3-D Secure Change card abonent'; 
      ELSIF PTRANCODE=611 THEN  RETURN 'Change 3DSecure authentication password for card'; 
      ELSIF PTRANCODE=621 THEN  RETURN 'Generate 3DSecure dynamic password'; 
      ELSIF PTRANCODE=622 THEN  RETURN 'Change card e-Commerce status when executing purchase via 3DSecure'; 
      ELSIF PTRANCODE=624 THEN  RETURN 'Authorization process';  
      END IF;
    ELSIF PTRANCLASS = TRANCMSCLASS THEN 
      IF    PTRANCODE=701 THEN  RETURN 'Payment';
      ELSIF PTRANCODE=702 THEN  RETURN 'Change card status';
      ELSIF PTRANCODE=703 THEN  RETURN 'Notification of balance';
      ELSIF PTRANCODE=704 THEN  RETURN 'Card expiration notification';
      ELSIF PTRANCODE=705 THEN  RETURN 'Send list of available commands to customer';
      ELSIF PTRANCODE=706 THEN  RETURN 'Send new approval code to customer';
      ELSIF PTRANCODE=707 THEN  RETURN 'Enable/Disable card notification';
      ELSIF PTRANCODE=708 THEN  RETURN 'Enable/Disable diverting of messages to additional address';
      ELSIF PTRANCODE=709 THEN  RETURN 'Approved transaction notification';
      ELSIF PTRANCODE=710 THEN  RETURN 'Declined transaction notification';
      ELSIF PTRANCODE=711 THEN  RETURN 'Balance change notification';
      ELSIF PTRANCODE=712 THEN  RETURN 'Change abonent dynamic profile at customer`s command';
      ELSIF PTRANCODE=713 THEN  RETURN 'Customer notification on receiving of message from bank via bank messaging service';
      ELSIF PTRANCODE=714 THEN  RETURN 'Delivery of bank message to customer';
      ELSIF PTRANCODE=715 THEN  RETURN 'Create message for bank in messaging service at customer`s command';
      ELSIF PTRANCODE=716 THEN  RETURN 'Change time maximum of card limit at customer`s command';
      ELSIF PTRANCODE=717 THEN  RETURN 'Send list of available personal payments linked to card to customer';
      ELSIF PTRANCODE=718 THEN  RETURN 'Send list of card limits possible to be changed to customer';
      ELSIF PTRANCODE=719 THEN  RETURN 'Customer notification on change of bonus/debt field in Refresh';
      ELSIF PTRANCODE=720 THEN  RETURN 'Send currency rates to customer';
      ELSIF PTRANCODE=721 THEN  RETURN 'Send list of currencies to customer';
      ELSIF PTRANCODE=722 THEN  RETURN 'Send notification of abonent subscription state change to customer';
      ELSIF PTRANCODE=723 THEN  RETURN 'Send history of transactions to customer';
      ELSIF PTRANCODE=724 THEN  RETURN 'Send list of transactions to customer';
      ELSIF PTRANCODE=740 THEN  RETURN 'Notify retailer of approved transaction';  
      ELSIF PTRANCODE=741 THEN  RETURN 'Notify retailer of declined transaction';  
      ELSIF PTRANCODE=751 THEN  RETURN 'Payment via messaging service (debit)';
      ELSIF PTRANCODE=752 THEN  RETURN 'Payment via messaging service (credit)';
      ELSIF PTRANCODE=756 THEN  RETURN 'Debit part of P2P transfer';
      ELSIF PTRANCODE=757 THEN  RETURN 'Credit part of P2P transfer';
      ELSIF PTRANCODE=799 THEN  RETURN 'Traffic transaction'; 
      END IF;
    ELSE
      RETURN 'Unknown transaction class: '||PTRANCLASS;
    END IF;
    RETURN 'Unknown transaction:'||PTRANCODE;
  END;




FUNCTION GETTRANCLASSNAME(PTRANCLASS IN PLS_INTEGER) RETURN VARCHAR
  IS
  BEGIN
    IF PTRANCLASS = TRANATMCLASS THEN
      RETURN 'ATM';
    ELSIF PTRANCLASS = TRANPOSCLASS THEN
      RETURN 'POS';
    ELSIF PTRANCLASS = TRANINTERCLASS THEN
      RETURN 'Interchange';
    ELSIF PTRANCLASS = TRANTBCLASS THEN
      RETURN 'TeleBank';
    ELSIF PTRANCLASS = TRANMANUALCLASS THEN
      RETURN 'Manual';
    ELSIF PTRANCLASS = TRANFIMICLASS THEN
      RETURN 'FIMI';
    ELSIF PTRANCLASS = TRANECOMMERCECLASS THEN
      RETURN 'ECommerce'; 
    ELSIF PTRANCLASS = TRANCMSCLASS THEN
      RETURN 'CMS';
    END IF;
    RETURN NULL;
  END;

FUNCTION  GETTRANTYPENAME(PTRANTYPE IN PLS_INTEGER) RETURN VARCHAR
  IS
  BEGIN
    IF PTRANTYPE=CAUTHREQUESTTYPE THEN
      RETURN 'Authorization';
    ELSIF PTRANTYPE=CAUTHADVICETYPE THEN
      RETURN 'Athorization advice';
    ELSIF PTRANTYPE=CREQUESTTYPE THEN
      RETURN 'Financial';
    ELSIF PTRANTYPE=CADVICETYPE THEN
      RETURN 'Financial advice';
    ELSIF PTRANTYPE=CREVERSALTYPE THEN
      RETURN 'Cancel';
    ELSIF PTRANTYPE=CISSUERREQUESTTYPE THEN
      RETURN 'ChargeBack';
    ELSIF PTRANTYPE=CISSUERREVERSALTYPE THEN
      RETURN 'Cancel ChargeBack';
    ELSIF PTRANTYPE=CADMINADVICETYPE THEN
      RETURN 'Admin. Advice';
    ELSIF PTRANTYPE=CADMINTYPE THEN
      RETURN 'Administrative';
    END IF;
    RETURN '???';
  END;




FUNCTION  GETEXTRACTTRANCODE(PTRANTYPE IN NUMBER,
                             PDEVICE   IN CHAR,
                             PTRANCODE IN NUMBER,
                             PRESPCODE IN NUMBER,
                             PAMOUNT IN NUMBER,
                             PADMININITIATOR IN NUMBER,
                             PREASON   IN NUMBER,
                             OTRANCODE OUT VARCHAR,
                             PTERMSIC IN NUMBER,
                             PCONVERTP11TOP10 BOOLEAN, 
                             PTRANTERMCLASS IN NUMBER,  
                             PTRANCATEGORY IN NUMBER,  
                             PTRANCLASS IN NUMBER,  
                             PDRAFTCAPTURE IN NUMBER,  
                             PCONVERTP12TOP10 IN BOOLEAN  
                            ) RETURN VARCHAR
 IS

   VCODE NUMBER;

   FUNCTION BIGNUM2CHAR(PCODE IN VARCHAR) RETURN VARCHAR
     IS
     BEGIN
       

       RETURN CHR(65 + TO_NUMBER(SUBSTR(PCODE, -4, 1) - 1) * 10 + TO_NUMBER(SUBSTR(PCODE, -2, 1))) || SUBSTR(PCODE, -1, 1);
       
     END;

 BEGIN
  S.SAY(CPACKAGENAME||'.GetExtractTranCode pTranType=['||PTRANTYPE||']');
  S.SAY(CPACKAGENAME||'.GetExtractTranCode pDevice=['||PDEVICE||']');
  S.SAY(CPACKAGENAME||'.GetExtractTranCode pTranCode=['||PTRANCODE||']');
  S.SAY(CPACKAGENAME||'.GetExtractTranCode pRespCode=['||PRESPCODE||']');
  S.SAY(CPACKAGENAME||'.GetExtractTranCode pAmount=['||PAMOUNT||']');
  S.SAY(CPACKAGENAME||'.GetExtractTranCode pAdminInitiator=['||PADMININITIATOR||']');
  S.SAY(CPACKAGENAME||'.GetExtractTranCode pReason=['||PREASON||']');
  S.SAY(CPACKAGENAME||'.GetExtractTranCode oTranCode=['||OTRANCODE||']');
  S.SAY(CPACKAGENAME||'.GetExtractTranCode pTermSIC=['||PTERMSIC||']');
  S.SAY(CPACKAGENAME||'.GetExtractTranCode pTranCategory=['||PTRANCATEGORY||']');

  IF PDEVICE=REFERENCETERMINAL.DEVICE_ATM THEN
    IF PTRANCODE = 86 THEN 
      VCODE := 1086;
    ELSIF PTRANCODE = 87 THEN 
      VCODE := 1087;
    END IF;

  ELSIF PDEVICE IN (REFERENCETERMINAL.DEVICE_POS,REFERENCETERMINAL.DEVICE_BANKPOS,REFERENCETERMINAL.DEVICE_VOICE) THEN
    IF PTRANCODE = 187 THEN 
      VCODE := 1187;
    END IF;

  ELSIF PDEVICE=REFERENCETERMINAL.DEVICE_FIMI THEN
    IF PTRANCODE = 199 THEN 
      VCODE := 99;  
    ELSIF PTRANCODE = 598 THEN 
      VCODE := 1098;
    ELSE
      VCODE := PTRANCODE-500;
    END IF;

  ELSIF PDEVICE=REFERENCETERMINAL.DEVICE_ECOMMERCE THEN
    VCODE := PTRANCODE-600; 

  ELSIF PDEVICE=REFERENCETERMINAL.DEVICE_CMS THEN
    IF PTRANCODE = 799 THEN 
      VCODE := 1099;
    ELSE
      VCODE := PTRANCODE-700;
    END IF;

  ELSIF PDEVICE=REFERENCETERMINAL.DEVICE_SYSTEM THEN
    IF PTRANCODE=402 THEN 
      VCODE := 2;
    ELSIF PTRANCODE=403 THEN 
      VCODE := 81;
    ELSIF PTRANCODE=405 THEN 
      VCODE := 5;
    ELSIF PTRANCODE=414 THEN 
      VCODE := 14;
    ELSIF PTRANCODE=423 THEN 
      VCODE := 23;
    ELSIF PTRANCODE=425 THEN 
      VCODE := 1025;
    ELSIF PTRANCODE=440 THEN 
      VCODE := 1040;
    ELSIF PTRANCODE=442 THEN 
      VCODE := 1042;
    ELSIF PTRANCODE=443 THEN
      VCODE := 43;
    ELSIF PTRANCODE=444 THEN 
      VCODE := 44;
    ELSIF PTRANCODE=446 THEN 
      VCODE := 46;
    ELSIF PTRANCODE=447 THEN 
      VCODE := 47;
    ELSIF PTRANCODE=460 THEN 
      VCODE := 60;
    ELSIF PTRANCODE=467 THEN 
      VCODE := 67;
    ELSIF PTRANCODE=472 THEN 
      VCODE := 72;
    ELSIF PTRANCODE=473 THEN 
      VCODE := 73;
    ELSIF PTRANCODE=482 THEN 
      VCODE := 82;
    ELSIF PTRANCODE=486 THEN 
      VCODE := 86;
    ELSIF PTRANCODE=487 THEN 
      VCODE := 87;
    END IF;
  END IF;

  
  IF PDEVICE IN (REFERENCETERMINAL.DEVICE_ATM, REFERENCETERMINAL.DEVICE_POS, REFERENCETERMINAL.DEVICE_BANKPOS, REFERENCETERMINAL.DEVICE_VOICE, REFERENCETERMINAL.DEVICE_TELEBANK) THEN
    IF PTRANCODE = 199 THEN
      VCODE := 1099;
    ELSIF PTRANCODE IN (803, 804, 805, 806, 807, 823, 824, 829, 830, 832) THEN  
      IF PDEVICE = REFERENCETERMINAL.DEVICE_TELEBANK THEN  
        VCODE := PTRANCODE+1200;
      ELSE
        VCODE := PTRANCODE+200;
      END IF;  
    END IF;
  END IF;
  

  S.SAY(CPACKAGENAME||'.GetExtractTranCode 1) vCode=['||VCODE||']');

  IF PTRANTYPE IN (CREQUESTTYPE,CAUTHREQUESTTYPE,CREVERSALTYPE,CAUTHADVICETYPE,CADVICETYPE) THEN
    IF PTRANTYPE = CADVICETYPE THEN
      
      IF PTRANCATEGORY IN (CTRANCAT_REPRESENTMENTADVICE, CTRANCAT_REPRESENTMENT) THEN 
        
        VCODE := 87;
      
      ELSIF ISREASONDISPUT(PREASON) THEN 
        
        VCODE := 87; 
      ELSE
        IF PTRANCODE IN (201, 202) THEN 
          VCODE := 0; 
        END IF;
      END IF;
    END IF;
  ELSIF PTRANTYPE IN (CISSUERREQUESTTYPE,CISSUERREVERSALTYPE) THEN 
    
    IF PTRANCODE = 64 THEN  
      VCODE := 88;
    ELSE  
    
      
      VCODE := 86; 
    END IF; 
  END IF;

  S.SAY(CPACKAGENAME||'.GetExtractTranCode 2) vCode=['||VCODE||']');

  IF VCODE IS NOT NULL THEN
    NULL;
  ELSIF PDEVICE=REFERENCETERMINAL.DEVICE_ATM THEN
    IF PTRANCODE = 71 THEN 
      VCODE:=70;
    ELSIF PTRANCODE IN (
      CBAOBABBALANCATM, CBAOBABINKASSATM,
      CBAOBABBALANCCUTOVERATM, CBAOBABINKASSATM1,
      CBAOBABOUTKASSATM, CBAOBABINKASSATMCASHIN) THEN
    














      IF PTRANCODE IN (CBAOBABBALANCATM,CBAOBABBALANCCUTOVERATM) THEN 
        VCODE:=5;
      ELSIF PTRANCODE = CBAOBABINKASSATM1 THEN 
        VCODE:=7;
      ELSIF PTRANCODE = CBAOBABOUTKASSATM THEN 
        VCODE:=3;
      ELSIF PTRANCODE = CBAOBABINKASSATM THEN 
        IF PAMOUNT>=0 THEN
          VCODE:=3;
        ELSE
          VCODE:=7;
        END IF;
      ELSIF PTRANCODE = CBAOBABINKASSATMCASHIN THEN 
        VCODE := 1;
      END IF;
      IF PADMININITIATOR!=0 THEN 
        VCODE:=VCODE+1;
      END IF;
    ELSE
      VCODE:=PTRANCODE;
    END IF;

  ELSIF PDEVICE IN (REFERENCETERMINAL.DEVICE_POS,REFERENCETERMINAL.DEVICE_BANKPOS,REFERENCETERMINAL.DEVICE_VOICE) THEN
    IF PTRANCODE BETWEEN 100 AND 199 THEN
      IF PTRANCODE = 140 AND PDEVICE = REFERENCETERMINAL.DEVICE_VOICE AND PTERMSIC = 6010 THEN 
        VCODE := 20;
      ELSIF PTRANCODE = 162 AND 
            PDEVICE != REFERENCETERMINAL.DEVICE_POS THEN 
        VCODE := 06;   
      ELSE
        VCODE:= PTRANCODE-CDELTAPOSTRANCODE;
        IF    VCODE=30 THEN VCODE:=30;
        
        ELSIF VCODE=41 THEN VCODE:=22;
        ELSIF VCODE=11 THEN 
          IF PCONVERTP11TOP10 THEN 
            VCODE := 10;
          END IF;
        ELSIF VCODE=12 THEN  
          
          IF PTRANTYPE IN (CAUTHADVICETYPE, CREVERSALTYPE) THEN
            IF PTRANCLASS IN (CLOCALTYPE, CACQTYPE) THEN
              IF PDRAFTCAPTURE = 2 AND PCONVERTP12TOP10 THEN
                VCODE := 10;
              END IF;
            END IF;  
          
          
          ELSIF PTRANTYPE = CAUTHREQUESTTYPE THEN
            IF PTRANCLASS IN (CLOCALTYPE, CACQTYPE) THEN
              IF PDRAFTCAPTURE = 2 THEN
                VCODE := 10;
              END IF;
            END IF;  
          
          ELSIF PTRANTYPE IN (CREQUESTTYPE, CADVICETYPE) THEN  
             VCODE := 10;
          END IF;   
        ELSIF VCODE=97 THEN VCODE:=99; 
        END IF;
      END IF;
    ELSIF PTRANCODE BETWEEN 1100 AND 1199 THEN 
      VCODE:= BIGNUM2CHAR(TO_CHAR(PTRANCODE-CDELTAPOSTRANCODE));
    END IF;
  ELSIF PDEVICE = REFERENCETERMINAL.DEVICE_TELEBANK THEN
    VCODE:=PTRANCODE-300;
  END IF;
S.SAY(CPACKAGENAME||'.GetExtractTranCode 3) vCode=['||VCODE||']');
  
  IF PDEVICE=REFERENCETERMINAL.DEVICE_SYSTEM AND PTRANCODE IN (205, 206) THEN
    VCODE := 20;
  END IF;
S.SAY(CPACKAGENAME||'.GetExtractTranCode 4) vCode=['||VCODE||']');
  IF VCODE BETWEEN 1000 AND 1199 OR VCODE >= 2000 THEN  
    OTRANCODE := BIGNUM2CHAR(TO_CHAR(VCODE)); 
  ELSIF VCODE BETWEEN 1 AND 99 THEN
    OTRANCODE:=LPAD(TO_CHAR(VCODE),2,'0');
  ELSIF VCODE = 0 THEN
    OTRANCODE := '00'; 
  ELSE
    ERR.SETERROR(ERR.A4MFO_UNKNOW_VALUE,CPACKAGENAME||'.GetExtractTranCode.Code');
    RETURN NULL;
  END IF;
S.SAY(CPACKAGENAME||'.GetExtractTranCode 1) oTranCode=['||OTRANCODE||']');
  
  



 
  
  
  
  IF PRESPCODE NOT IN (RESPONSE.APPROVED, RESPONSE.APPROVEDPARTIAL, RESPONSE.APPROVEDPURCHASEONLY) AND
     (PTRANTYPE != CADMINTYPE OR PTRANCODE IN (528, 529)) AND NOT (PTRANTERMCLASS = TRANINTERCLASS AND PTRANCODE IN (205, 206)) THEN 
  
    ERR.SETERROR(ERR.A4MFO_NOT_APPROVED,CPACKAGENAME||'.GetExtractTranCode');
    S.SAY(CPACKAGENAME||'.GetExtractTranCode 0) Tran is not approved!'); 
    IF PDEVICE IN (REFERENCETERMINAL.DEVICE_ATM,REFERENCETERMINAL.DEVICE_BANKPOS) THEN
      RETURN '99';
    ELSE
      RETURN '98';
    END IF;
  END IF;
  
  S.SAY(CPACKAGENAME||'.GetExtractTranCode 2) oTranCode=['||OTRANCODE||']');
  RETURN OTRANCODE;
 EXCEPTION
  WHEN OTHERS THEN
    ERROR.SAVE(CPACKAGENAME||'.GetExtractTranCode');
    RAISE;
 END;




FUNCTION GETEXTRACTENTTYPEPRE(PTLGTYPE IN PLS_INTEGER,
                              PEXTRACTDEVICE IN VARCHAR,
                              PEXTRACTENTCODE IN VARCHAR,
                              PTRANCLASS IN PLS_INTEGER,
                              PDRAFTCAPTURE IN PLS_INTEGER,
                              PORGTLGTYPE  IN PLS_INTEGER,
                              PFTWRAUTORIZED IN BOOLEAN,
                              PTRANCODE IN PLS_INTEGER,
                              PTRANCATEGORY IN NUMBER) RETURN CHAR  
  IS
    VRET CHAR(1);
  BEGIN

    









    IF PEXTRACTDEVICE||PEXTRACTENTCODE='T20' THEN 
      RETURN EXTRACT.ENTTYPE_SINGLE;
    END IF;


    IF PTLGTYPE = CREVERSALTYPE THEN
      IF PORGTLGTYPE IN (CAUTHREQUESTTYPE,CAUTHADVICETYPE) THEN
        IF PTRANCLASS NOT IN (CACQTYPE, CLOCALTYPE) THEN
          RETURN EXTRACT.ENTTYPE_CANCEL;
        END IF;
      END IF;

      
      IF PTRANCLASS IN (CLOCALTYPE, CACQTYPE) THEN
        
        IF PDRAFTCAPTURE = 0 THEN
          RETURN EXTRACT.ENTTYPE_CANCEL;
        ELSIF PDRAFTCAPTURE IS NULL THEN
          IF PEXTRACTDEVICE = REFERENCETERMINAL.DEVICE_VOICE THEN
            RETURN EXTRACT.ENTTYPE_CANCEL; 
          ELSIF PEXTRACTDEVICE||PEXTRACTENTCODE='A20' THEN 
            RETURN EXTRACT.ENTTYPE_CANCEL; 
          END IF;
        END IF;
      END IF;
      

      RETURN EXTRACT.ENTTYPE_REVERSAL;
    ELSIF PTLGTYPE= CISSUERREQUESTTYPE THEN
      RETURN EXTRACT.ENTTYPE_SINGLE;
    ELSIF PTLGTYPE= CISSUERREVERSALTYPE THEN
      RETURN EXTRACT.ENTTYPE_REVERSAL;
    ELSIF PTLGTYPE IN ( CREQUESTTYPE,
                        CAUTHREQUESTTYPE,
                        CADVICETYPE,
                        CAUTHADVICETYPE) THEN
      
      IF PEXTRACTENTCODE IN ('98','99') THEN 
        RETURN EXTRACT.ENTTYPE_SINGLE;
      END IF;
      S.SAY('1804-2',4);
      IF PTRANCLASS IN (CISSTYPE,        
                        CALIENTYPE) THEN 
        IF PTLGTYPE = CREQUESTTYPE THEN              
          S.SAY('|1800-2',4);
          VRET:= EXTRACT.ENTTYPE_SINGLE;
        ELSIF PTLGTYPE = CAUTHREQUESTTYPE THEN 
          S.SAY('|1800-3',4);
          VRET:= EXTRACT.ENTTYPE_AUTHORIZATION;
        ELSIF PTLGTYPE= CADVICETYPE THEN      
          
          IF PTRANCATEGORY = CTRANCAT_BASEIIADVICE THEN
            S.SAY('|1800-4 BaseII',4);
            VRET:= EXTRACT.ENTTYPE_FINANCE;
          ELSE
          
            S.SAY('|1800-4',4);
            VRET:= EXTRACT.ENTTYPE_SINGLE;
          END IF;  
        ELSIF PTLGTYPE = CAUTHADVICETYPE THEN  
          S.SAY('|1800-5',4);
          VRET:= EXTRACT.ENTTYPE_AUTHORIZATION;
        END IF;

      ELSIF PTRANCLASS IN (CACQTYPE, CLOCALTYPE) THEN 
        S.SAY('|1800-6',4);
        VRET:= EXTRACT.ENTTYPE_SINGLE;
      END IF;

        S.SAY('|1800-7: '||PTRANCLASS,4);
      
      IF PTRANCLASS IN (CLOCALTYPE,CACQTYPE) THEN 
        S.SAY('|1800-71: our terminal, DCf:'||PDRAFTCAPTURE,4);
        IF PDRAFTCAPTURE=2 THEN 
          S.SAY('|1800-71|draftcapture=2',4);
          VRET:= EXTRACT.ENTTYPE_FINANCE;
        ELSE
          
          IF PDRAFTCAPTURE IS NOT NULL THEN
              S.SAY('|1800-73|FlagUseDraftCapture',4);
            IF PDRAFTCAPTURE=0 THEN  
              S.SAY('|1800-74|draftcapture=0',4);
              VRET := EXTRACT.ENTTYPE_AUTHORIZATION;
            END IF;
          ELSE 
              S.SAY('|1800-75|not FlagUseDraftCapture',4);
            IF PEXTRACTDEVICE = REFERENCETERMINAL.DEVICE_VOICE THEN
              S.SAY('|1800-75|TermType=VOICE',4);
              VRET := EXTRACT.ENTTYPE_AUTHORIZATION; 
            ELSIF PEXTRACTDEVICE||PEXTRACTENTCODE='A20' THEN 
              S.SAY('|1800--ATMdeposit',4);
              VRET := EXTRACT.ENTTYPE_AUTHORIZATION; 
            END IF;
          END IF;
        END IF;
      END IF;

    ELSIF PTLGTYPE IN (CADMINTYPE,CADMINADVICETYPE) THEN
      S.SAY('1804-1',4);
      VRET := EXTRACT.ENTTYPE_SINGLE;
    END IF;
    RETURN VRET;
  EXCEPTION
   WHEN OTHERS THEN
     ERROR.SAVE(CPACKAGENAME||'.GetExtractEntTypePre');
     RAISE;
  END;




FUNCTION GETEXTRACTENTTYPE(PTLGTYPE IN PLS_INTEGER,
                           PEXTRACTDEVICE IN VARCHAR,
                           PEXTRACTENTCODE IN VARCHAR,
                           PTRANCLASS IN PLS_INTEGER,
                           PDRAFTCAPTURE IN PLS_INTEGER,
                           PORGTLGTYPE  IN PLS_INTEGER,
                           PFTWRAUTORIZED IN BOOLEAN:=FALSE,
                           PTRANCODE IN PLS_INTEGER:=NULL,
                           PTRANCATEGORY IN NUMBER:=NULL) RETURN CHAR  
  IS
    VRET CHAR(1);
  BEGIN

    
    IF PTRANCODE = 111 OR 
       PTRANCODE = 124 THEN 
      
      IF PTLGTYPE = CREVERSALTYPE THEN
        VRET :=  EXTRACT.ENTTYPE_CANCEL;
      ELSE 
        VRET := EXTRACT.ENTTYPE_AUTHORIZATION;
      END IF; 
    ELSIF PTRANCODE = 112 THEN 
      IF PTLGTYPE = CAUTHADVICETYPE THEN
        IF PEXTRACTDEVICE||PEXTRACTENTCODE = 'P12' THEN  
          VRET := EXTRACT.ENTTYPE_AUTHORIZATION;
        END IF;  
      ELSIF PTLGTYPE IN (CREQUESTTYPE, CADVICETYPE) THEN 
        VRET := EXTRACT.ENTTYPE_FINANCE;
      
      ELSIF PTLGTYPE = CREVERSALTYPE THEN
        IF PEXTRACTDEVICE||PEXTRACTENTCODE = 'P12' THEN
          VRET := EXTRACT.ENTTYPE_CANCEL;
        END IF;
      
      END IF;
    
    ELSIF PTRANCODE = 807 THEN 
      IF PTLGTYPE IN (CREQUESTTYPE, CADVICETYPE) THEN
        VRET := EXTRACT.ENTTYPE_FINANCE;
      END IF; 
    END IF;

    IF VRET IS NULL THEN 
      VRET := GETEXTRACTENTTYPEPRE(PTLGTYPE, PEXTRACTDEVICE, PEXTRACTENTCODE, PTRANCLASS, PDRAFTCAPTURE, PORGTLGTYPE, PFTWRAUTORIZED, PTRANCODE, PTRANCATEGORY);  
    END IF; 

    
    IF PEXTRACTDEVICE || PEXTRACTENTCODE IN ('A30','B17','P17','V17','B16','P16', 'P78', 'P79', 'P88', 'P89') THEN 
      IF VRET = EXTRACT.ENTTYPE_AUTHORIZATION THEN
        VRET := EXTRACT.ENTTYPE_SINGLE;
      ELSIF  VRET = EXTRACT.ENTTYPE_CANCEL THEN
        VRET := EXTRACT.ENTTYPE_REVERSAL;
      END IF;
    END IF;

    IF PEXTRACTDEVICE || PEXTRACTENTCODE = 'A81' AND VRET = EXTRACT.ENTTYPE_AUTHORIZATION THEN
      VRET := EXTRACT.ENTTYPE_SINGLE;
    END IF;

    RETURN VRET;

  END;




FUNCTION GETEXTACCOUNTNO(PACCOUNTNO IN VARCHAR) RETURN VARCHAR
  IS
    VACCOUNTNO VARCHAR(35);
  BEGIN
    IF PACCOUNTNO IS NULL THEN
      RETURN NULL;
    END IF;
    VACCOUNTNO := ACCOUNT.GETEXTERNALACCOUNTNUMBER(PACCOUNTNO); 
    IF VACCOUNTNO IS NULL THEN
      ERROR.RAISEERROR(ERR.ACCOUNT_EXTERNAL_UNDEFINED, PACCOUNTNO);
    END IF;
    RETURN VACCOUNTNO;
  EXCEPTION
    WHEN OTHERS THEN
      ERROR.SAVE(CPACKAGENAME||'.GetExtAccountNo');
      RAISE;
  END;




FUNCTION GETINTACCOUNTNO(PACCOUNTNO IN VARCHAR) RETURN VARCHAR
  IS
    VACCOUNTNO VARCHAR(20);
  BEGIN
    IF PACCOUNTNO IS NULL OR PACCOUNTNO = '?' OR PACCOUNTNO = '0' THEN
      RETURN NVL(VACCOUNTNO, '0');
    END IF;
    VACCOUNTNO := ACCOUNT.GETINTERNALACCOUNTNUMBER(PACCOUNTNO); 
    IF VACCOUNTNO IS NULL THEN
      ERROR.RAISEERROR(ERR.ACCOUNT_NOT_FOUND, PACCOUNTNO);
    END IF;
    RETURN VACCOUNTNO;
  EXCEPTION
    WHEN OTHERS THEN
      ERROR.SAVE(CPACKAGENAME||'.GetIntAccountNo');
      RAISE;
  END;




FUNCTION GETEXTIDCLIENT(PIDCLIENT IN NUMBER) RETURN VARCHAR 
  IS
    VIDCLIENT VARCHAR(50); 
  BEGIN
    VIDCLIENT := CLIENT.GETPERSONERECORD(PIDCLIENT).EXTERNALID;
    IF VIDCLIENT IS NULL THEN
      ERROR.RAISEERROR(ERR.CLIENT_NOT_FOUND, PIDCLIENT);
    END IF;
    RETURN VIDCLIENT;
  EXCEPTION
    WHEN OTHERS THEN
      ERROR.SAVE(CPACKAGENAME||'.GetExtIdClient');
      RAISE;
  END;




FUNCTION GETINTIDCLIENT(PEXTERNALID IN VARCHAR) RETURN NUMBER 
  IS
    VIDCLIENT NUMBER;
  BEGIN
    VIDCLIENT := CLIENT.GETPERSONIDBYEXTID(PEXTERNALID);
    IF VIDCLIENT IS NULL THEN
      ERROR.RAISEERROR(ERR.CLIENT_NOT_FOUND, PEXTERNALID);
    END IF;
    RETURN VIDCLIENT;
  EXCEPTION
    WHEN OTHERS THEN
      ERROR.SAVE(CPACKAGENAME||'.GetIntIdClient');
      RAISE;
  END;




FUNCTION GETPACKAGEREVISION RETURN VARCHAR2 IS
  BEGIN
    RETURN COALESCE($$PLSQL_UNIT, CPACKAGENAME)||'~'||
           TRIM(SUBSTR(CPACKAGEBODYREV, 6, LENGTH(CPACKAGEBODYREV) - 7))||' - '||TRIM(SUBSTR(CPACKAGEBODYREVDATE, 8, 20))||'~'||
           TRIM(SUBSTR(CPACKAGEREV, 6, LENGTH(CPACKAGEREV) - 7))||' - '||TRIM(SUBSTR(CPACKAGEREVDATE, 8, 20));
  END;




FUNCTION GETVERSION RETURN VARCHAR
  IS
  BEGIN
    RETURN SERVICE.FORMATPACKAGEVERSION(CPACKAGENAME, CPACKAGEREV, CPACKAGEBODYREV); 
  END;

END;
